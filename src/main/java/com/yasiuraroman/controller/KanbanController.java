package com.yasiuraroman.controller;

import com.yasiuraroman.model.Task;

import java.util.List;

public interface KanbanController {

    boolean addNewTask(String name);

    List<String> showAllTasksWithState();

    boolean selectTask(String name);

    Task getSelectedTask();

    void moveToCodeReview();
    void moveToDone();
    void moveToInProgress();
    void moveToToDo();
}
