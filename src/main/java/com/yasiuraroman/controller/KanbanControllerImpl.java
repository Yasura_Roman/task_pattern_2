package com.yasiuraroman.controller;

import com.yasiuraroman.model.Developer;
import com.yasiuraroman.model.Task;
import com.yasiuraroman.model.stateimpl.ToDoState;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class KanbanControllerImpl implements KanbanController {

    private Developer developer;

    public KanbanControllerImpl() {
        developer = new Developer();
    }

    @Override
    public boolean addNewTask(String name) {
        developer.addTask(new Task(name,new ToDoState()));
        return true;
    }

    @Override
    public List<String> showAllTasksWithState() {
        List<String> output = new ArrayList<>();
        for (Task t: developer.getTasks()
             ) {
            output.add(t.getName() + "\t" + t.getState().getClass().getName() + ";");
        }
        return output;
    }

    @Override
    public boolean selectTask(String name) {
        Optional<Task> task = developer.getTasks().stream().filter(e -> e.getName().equals(name)).findFirst();
        if (task.isPresent()){
            developer.setSelectedTask(task.get());
            return true;
        }
        return false;
    }

    @Override
    public Task getSelectedTask() {
        return developer.getSelectedTask();
    }

    @Override
    public void moveToCodeReview() {
        developer.moveToCodeReview();
    }

    @Override
    public void moveToDone() {
        developer.moveToDone();
    }

    @Override
    public void moveToInProgress() {
        developer.moveToInProgress();
    }

    @Override
    public void moveToToDo() {
        developer.moveToToDo();
    }
}
