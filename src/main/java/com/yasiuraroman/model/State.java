package com.yasiuraroman.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface State {
    Logger logger = LogManager.getLogger();
    
    default void moveToToDo(Task task){
        logger.info("moveToToDoState is not allowed");
    }

    default void moveToInProgress(Task task){
        logger.info("moveToInProgressState is not allowed");
    }

    default void moveToCodeReview(Task task){
        logger.info("moveToCodeReviewState is not allowed");
    }

    default void moveToDone(Task task){
        logger.info("moveToDoneState is not allowed");
    }
}
