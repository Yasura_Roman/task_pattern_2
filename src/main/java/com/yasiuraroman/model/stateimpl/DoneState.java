package com.yasiuraroman.model.stateimpl;

import com.yasiuraroman.model.Task;
import com.yasiuraroman.model.State;

public class DoneState implements State {
    @Override
    public void moveToDone(Task task) {
        State.logger.info("you in 'done' state");
    }
}
