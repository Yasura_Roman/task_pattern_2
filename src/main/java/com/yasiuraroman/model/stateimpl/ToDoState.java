package com.yasiuraroman.model.stateimpl;

import com.yasiuraroman.model.Task;
import com.yasiuraroman.model.State;

public class ToDoState implements State {
    @Override
    public void moveToToDo(Task task) {
        State.logger.info("you in 'to do' state");
    }

    @Override
    public void moveToInProgress(Task task) {
        task.setState(new InProgressState());
        State.logger.info("now you in 'in progress' state");
    }
}
