package com.yasiuraroman.model.stateimpl;

import com.yasiuraroman.model.Task;
import com.yasiuraroman.model.State;

public class CodeReviewState implements State {
    @Override
    public void moveToToDo(Task task) {
        task.setState(new ToDoState());
        State.logger.info("now you in 'to do' state");
    }

    @Override
    public void moveToInProgress(Task task) {
        task.setState(new InProgressState());
        State.logger.info("now you in 'in progress' state");
    }

    @Override
    public void moveToCodeReview(Task task) {
        State.logger.info("you in 'code review' state");
    }

    @Override
    public void moveToDone(Task task) {
        task.setState(new DoneState());
        State.logger.info("now you in 'done' state");
    }
}
