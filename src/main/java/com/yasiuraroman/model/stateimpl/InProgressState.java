package com.yasiuraroman.model.stateimpl;

import com.yasiuraroman.model.Task;
import com.yasiuraroman.model.State;

public class InProgressState implements State {
    @Override
    public void moveToToDo(Task task) {
        task.setState(new ToDoState());
        State.logger.info("now you in 'to do' state");
    }

    @Override
    public void moveToInProgress(Task task) {
        State.logger.info("you in 'in progress' state");
    }

    @Override
    public void moveToCodeReview(Task task) {
        task.setState(new CodeReviewState());
        State.logger.info("now you in 'code review' state");
    }
}
