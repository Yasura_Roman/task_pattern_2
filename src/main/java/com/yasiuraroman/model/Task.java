package com.yasiuraroman.model;

public class Task {
    private String name;
    private State state;

    public Task(String name, State state) {
        this.name = name;
        this.state = state;
    }

    public void setState(State state){
        this.state = state;
    }

    public void moveToToDo(){
        state.moveToToDo(this);
    }

    public void moveToInProgress(){
        state.moveToInProgress(this);
    }

    public void moveToCodeReview(){
        state.moveToCodeReview(this);
    }

    public void moveToDone(){
        state.moveToDone(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public State getState() {
        return state;
    }
}
