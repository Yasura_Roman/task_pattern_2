package com.yasiuraroman.model;

import java.util.ArrayList;
import java.util.List;

public class Developer {

    private Task selectedTask;
    private List<Task> tasks;

    public Developer() {
        tasks = new ArrayList<>();
    }

    public void addTask(Task task){
        tasks.add(task);
    }

    public Task getSelectedTask() {
        return selectedTask;
    }

    public void setSelectedTask(Task selectedTask) {
        this.selectedTask = selectedTask;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void moveToToDo(){
        selectedTask.moveToToDo();
    }

    public void moveToInProgress(){
        selectedTask.moveToInProgress();
    }

    public void moveToCodeReview(){
        selectedTask.moveToCodeReview();
    }

    public void moveToDone(){
        selectedTask.moveToDone();
    }

}
