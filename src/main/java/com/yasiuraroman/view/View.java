package com.yasiuraroman.view;

import com.yasiuraroman.controller.KanbanController;
import com.yasiuraroman.controller.KanbanControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class View {
    private Logger logger = LogManager.getLogger(View.class.getName());
    private Scanner input = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private KanbanController controller;
    private ResourceBundle menuBundle = ResourceBundle.getBundle("Menu");

    public View() {
        controller = new KanbanControllerImpl();
        createMenu();
        initMenuCommands();
    }

    private void createMenu(){
        menu = new LinkedHashMap<>();
        SortedSet<String> sortedSet = new TreeSet<>();
        sortedSet.addAll(menuBundle.keySet());
        for (String key : sortedSet
        ) {
            menu.put(key,menuBundle.getString(key));
        }
    }

    //-------------------------------------------------------------------------
    private void initMenuCommands() {
        methodsMenu = new HashMap<>();
        methodsMenu.put("1", this::addNewTask);
        methodsMenu.put("2", this::showAllTasksWithState);
        methodsMenu.put("3", this::selectTask);
        methodsMenu.put("4", this::moveToCodeReview);
        methodsMenu.put("5", this::moveToDone);
        methodsMenu.put("6", this::moveToInProgress);
        methodsMenu.put("7", this::moveToToDo);
    }

    private void addNewTask() {
        if(controller.addNewTask(input.nextLine())){
            logger.info("new task add");
        }
    }

    private void showAllTasksWithState() {
        for (String s:controller.showAllTasksWithState()
             ) {
            logger.info(s);
        }
    }

    private void selectTask() {
        if(controller.selectTask(input.nextLine())){
            logger.info(controller.getSelectedTask().getName());
        }else {
            logger.info("task not select");
        }
    }

    private void moveToCodeReview() {
        controller.moveToCodeReview();
    }

    private void moveToDone() {
        controller.moveToDone();
    }

    private void moveToInProgress() {
        controller.moveToInProgress();
    }

    private void moveToToDo() {
        controller.moveToToDo();
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
